<?php

namespace App\Http\Controllers\PajakKendaraan;

use App\Http\Controllers\Controller;
use DiDom\Document;
use Illuminate\Http\Request;

class PajakKendaraanAcehCont extends Controller
{
    public function getPajak($nopol = 'BL1223LN', Request $request)
    {
        // $sewil = (isset($request->sewil) ? $request->sewil : $sewil);
        // $nopol = (isset($request->nopol) ? $request->nopol : $nopol);
        // $seri = (isset($request->seri) ? $request->seri : $seri);

        // $param = 'Sewil=' . $sewil . '&NoPol=' . $nopol . '&Seri=' . $seri . '';

        //  get token from html
        $document = new Document('https://esamsat.acehprov.go.id/', true);

        $token = $document->find('input[name=_token]')[0]->value;
          $param = "_token=$token&nopol=$nopol";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://esamsat.acehprov.go.id/cdata",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $param,
            CURLOPT_HTTPHEADER => array(
                "authority: esamsat.acehprov.go.id",
                "cache-control: max-age=0",
                "upgrade-insecure-requests: 1",
                "origin: https://esamsat.acehprov.go.id",
                "content-type: application/x-www-form-urlencoded",
                "user-agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36",
                "accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "sec-fetch-site: same-origin",
                "sec-fetch-mode: navigate",
                "sec-fetch-user: ?1",
                "sec-fetch-dest: document",
                "referer: https://esamsat.acehprov.go.id/",
                "accept-language: en-US,en;q=0.9",

            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}
